# -*- coding: utf-8 -*-

# improt des modules
import sys
import pprint
from argparse import ArgumentParser
from  subprocess import check_output, CalledProcessError

# Arguement
parser = ArgumentParser(description='Simple ssh exemple with ls')
requiredNamed = parser.add_argument_group('required named arguments')
requiredNamed.add_argument('-u', '--user', dest='user', required=True,
                           help='Application name')
requiredNamed.add_argument('-r', '--remote', dest='remote', required=True,
                           help='Domain name')

# Preparation des variable d'argument
args = parser.parse_args()
user = args.user
remote = args.remote

# variable
rc = 0
pp = pprint.PrettyPrinter(indent=4)

# Preparation de la commande ssh
cmd = 'ls'
ssh = 'ssh ' + user + '@' + remote + ' "' + cmd + '"'
print 'Ma commande ssh est : '
print '   ' + ssh

# Execuution de la commande ssh

try:
    # On recupère la stdout du ssh dans la liste ssh_output
    ssh_output = check_output(ssh, shell=True).splitlines()
    print '\nRetour de la commande ssh :'
    pp.pprint(ssh_output)
except CalledProcessError as e:
    # On gere une erreur eventuelle de la commande ssh
    rc = e.returncode

# Sortie du script python
sys.exit(rc)
